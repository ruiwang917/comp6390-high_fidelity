package com.example.high_fidelity_prototype.data;

import android.app.Application;

import com.example.high_fidelity_prototype.model.Dish;

import java.util.ArrayList;

public class DishData extends Application {
    private static ArrayList <Dish> Dishes = new ArrayList<>();

    public DishData(){
        initData();
    }

    public static ArrayList<Dish> getDishes() {
        return Dishes;
    }

    public static void setDishes(ArrayList<Dish> dishes) {
        Dishes = dishes;
    }

    private void initData() {
        // All dishes

        // Dish 0: Fried Rice
        // https://www.allrecipes.com/recipe/79543/fried-rice-restaurant-style/
        int id0 = 0;
        String name0 = "fried rice";
        String image0= "https://www.allrecipes.com/thmb/BiWSuzV8rgP7ShH67xvohdgje3o=/1500x0/filters:no_upscale():max_bytes(150000):strip_icc():format(webp)/79543-fried-rice-restaurant-style-mfs-51-155e83b4e4444e2292707287a56ddd93.jpg";
        int prep0 = 5;
        int cook0 = 15;
        int total0 = 20;
        int servings0 = 8;

        // ingredients
        String ingrenients0 = "⅔ cup chopped baby carrots\n"
                + "½ cup frozen green peas\n"
                + "2 tablespoons vegetable oil\n"
                + "1 clove garlic, minced, or to taste (Optional)\n"
                + "2 large eggs\n"
                + "3 cups leftover cooked white rice\n"
                + "1 tablespoon soy sauce, or more to taste\n"
                + "2 teaspoons sesame oil, or to taste\n";

        // instructions
        ArrayList<String> instructions0 = new ArrayList<>();
        instructions0.add("Place carrots in a small saucepan and cover with water. Bring to a low boil and cook for 3 to 5 minutes. Stir in peas, then immediately drain in a colander.");
        instructions0.add("Heat a wok over high heat. Pour in vegetable oil, then stir in carrots, peas, and garlic; cook for about 30 seconds. Add eggs; stir quickly to scramble eggs with vegetables.");
        instructions0.add("Stir in cooked rice. Add soy sauce and toss rice to coat. Drizzle with sesame oil and toss again.");

        Dish dish0 = new Dish();
        dish0.setId(id0);
        dish0.setName(name0);
        dish0.setImg(image0);
        dish0.setPrep(prep0);
        dish0.setCook(cook0);
        dish0.setTotal(total0);
        dish0.setServing(servings0);
        dish0.setIngredients(ingrenients0);
        dish0.setInstructions(instructions0);
        Dishes.add(dish0);



        // Dish 1: Juicy Roasted Chicken
        // https://www.allrecipes.com/recipe/83557/juicy-roasted-chicken/

        int id1 = 1;
        String name1 = "Juicy Roasted Chicken";
        String image1 = "https://www.allrecipes.com/thmb/OWtW645TgxYL1juIjcT-77SLAtg=/1500x0/filters:no_upscale():max_bytes(150000):strip_icc():format(webp)/83557-juicy-roast-chicken-mfs495-1-3d0f1e3e54104d43b24d4b480d2839ba.jpg";
        int prep1 = 15;
        int cook1 = 75;
        int total1 = 90;
        int servings1 = 6;

        // ingredients
        String ingrenients1 = "1 (3 pound) whole chicken, giblets removed\n"
                + "salt and black pepper to taste\n"
                + "1 tablespoon onion powder, or to taste\n"
                + "½ cup butter or margarine.\n"
                + "1 stalk celery, leaves removed\n";

        // instructions
        ArrayList<String> instructions1 = new ArrayList<>();
        instructions1.add("Preheat the oven to 350 degrees F (175 degrees C).");
        instructions1.add("Place chicken in a roasting pan; season generously inside and out with onion powder, salt, and pepper. Place 3 tablespoons of butter in chicken cavity; arrange dollops of remaining butter on the outside of chicken. Cut celery into 3 or 4 pieces; place in the chicken cavity.");
        instructions1.add("Bake chicken uncovered in the preheated oven until no longer pink at the bone and the juices run clear, about 1 hour and 15 minutes. An instant-read thermometer inserted into the thickest part of the thigh, near the bone, should read 165 degrees F (74 degrees C).");
        instructions1.add("Remove from the oven and baste with drippings. Cover with aluminum foil and allow to rest for about 30 minutes before serving.");

        Dish dish1 = new Dish();
        dish1.setId(id1);
        dish1.setName(name1);
        dish1.setImg(image1);
        dish1.setPrep(prep1);
        dish1.setCook(cook1);
        dish1.setTotal(total1);
        dish1.setServing(servings1);
        dish1.setIngredients(ingrenients1);
        dish1.setInstructions(instructions1);
        Dishes.add(dish1);



        // Dish 2: Slow Cooker Texas Pulled Pork
        // https://www.allrecipes.com/recipe/92462/slow-cooker-texas-pulled-pork/

        int id2 = 2;
        String name2 = "Slow Cooker Texas Pulled Pork\n";
        String image2 = "https://www.allrecipes.com/thmb/P0nJzsrbCuKeIBKgRgkrRZMmyTU=/1500x0/filters:no_upscale():max_bytes(150000):strip_icc():format(webp)/92462_Slow-Cooker-Texas-Pulled-Pork_psychedilemma_4560053_original-1x1-1-e3c3aa5d5a10431b8b5cb589174c0060.jpg";
        int prep2 = 15;
        int cook2 = 300;
        int total2 = 315;
        int servings2 = 8;

        // ingredients
        String ingrenients2 = "1 teaspoon vegetable oil\n"
                + "1 (4 pound) pork shoulder roast\n"
                + "1 cup barbeque sauce\n"
                + "½ cup butter or margarine.\n"
                + "½ cup apple cider vinegar\n"
                + "½ cup chicken broth\n"
                + "¼ cup light brown sugar\n"
                + "1 tablespoon prepared yellow mustard\n"
                + "1 tablespoon Worcestershire sauce\n"
                + "1 tablespoon chili powder\n"
                + "1 extra large onion, chopped\n"
                + "2 large cloves garlic, crushed\n"
                + "1 ½ teaspoons dried thyme\n"
                + "8 hamburger buns, split\n";

        // instructions
        ArrayList<String> instructions2 = new ArrayList<>();
        instructions2.add("Pour vegetable oil into the bottom of a slow cooker. Place pork roast into the slow cooker; pour in barbecue sauce, vinegar, and chicken broth. Stir in brown sugar, yellow mustard, Worcestershire sauce, chili powder, onion, garlic, and thyme. Cover and cook on Low for 10 to 12 hours or High for 5 to 6 hours until pork shreds easily with a fork.");
        instructions2.add("Remove pork from the slow cooker, and shred the meat using two forks. Return shredded pork to the slow cooker, and stir to combine with juices.");
        instructions2.add("Spread the inside of both halves of hamburger buns with butter. Toast buns, butter-side down, in a skillet over medium heat until golden brown. Spoon pulled pork into toasted buns.");

        Dish dish2 = new Dish();
        dish2.setId(id2);
        dish2.setName(name2);
        dish2.setImg(image2);
        dish2.setPrep(prep2);
        dish2.setCook(cook2);
        dish2.setTotal(total2);
        dish2.setServing(servings2);
        dish2.setIngredients(ingrenients2);
        dish2.setInstructions(instructions2);
        Dishes.add(dish2);


    }
}
