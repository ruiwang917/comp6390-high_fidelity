package com.example.high_fidelity_prototype;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.high_fidelity_prototype.data.DishData;
import com.example.high_fidelity_prototype.model.Dish;

import java.util.ArrayList;

public class Instructions extends AppCompatActivity implements View.OnClickListener {

    private TextView stepNumber, stepDeatil;
    private ImageButton back, next;

    DishData dishData = (DishData) this.getApplication();
    ArrayList<Dish> dishList;
    private int currentIndex = 0;
    int size;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_instructions);
        stepNumber = findViewById(R.id.stepName);
        stepDeatil = findViewById(R.id.stepDetail);
        back = findViewById(R.id.backInstruc);
        next = findViewById(R.id.nextnstruc);
        dishList = dishData.getDishes();
        size = getSize();
        setContent();
        setClickListeners();
    }

    private void setClickListeners() {
        back.setOnClickListener(this);
        next.setOnClickListener(this);
    }

    private void setContent(){
        int id = getIntent().getExtras().getInt("id");

        for(Dish d: dishList){
            if(d.getId() == id) {
                stepNumber.setText("Step: " + String.valueOf(currentIndex+1) +" / " + d.getInstructions().size());
                stepDeatil.setText(d.getInstructions().get(currentIndex));
            }
        }

    }

    private int getSize(){
        int id = getIntent().getExtras().getInt("id");

        for(Dish d: dishList){
            if(d.getId() == id) {
                size = d.getInstructions().size();

            }
        }
        return size;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.backInstruc: {
                if (currentIndex ==0) {
                    finish();
                }
                else{
                    currentIndex -= 1;
                    setContent();
                }
                break;
            }

            case R.id.nextnstruc: {
                if (currentIndex == size - 1) {
                    Intent toMain = new Intent(Instructions.this, MainActivity.class);
                    startActivity(toMain);
                }

                else{
                    currentIndex += 1;
                    setContent();
                }
                break;
            }
        }
    }

}