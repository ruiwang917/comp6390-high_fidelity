package com.example.high_fidelity_prototype;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.high_fidelity_prototype.data.DishData;
import com.example.high_fidelity_prototype.model.Dish;

import java.util.ArrayList;

public class Description extends AppCompatActivity implements View.OnClickListener {

    private ImageButton back, next;
    private ImageView dishImg;
    private TextView dishName, ingredientsDetails, prepText, cookText, totalText, servingText;

    DishData dishData = (DishData) this.getApplication();
    ArrayList<Dish> dishList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_description);

        back = findViewById(R.id.backInstruc);
        next = findViewById(R.id.nextnstruc);
        dishName = findViewById(R.id.dishName);
        dishImg = findViewById(R.id.dishImage);
        prepText = findViewById(R.id.PrepTime);
        cookText = findViewById(R.id.CookTime);
        totalText = findViewById(R.id.TotalTime);
        servingText = findViewById(R.id.Servings);
        ingredientsDetails = findViewById(R.id.ingredientsDetails);
        dishList = dishData.getDishes();
        setClickListeners();
        setContent();


    }

    private void setClickListeners() {
        back.setOnClickListener(this);
        next.setOnClickListener(this);
    }

    private void setContent(){
        int id = getIntent().getExtras().getInt("id");
        for(Dish d: dishList){
            if(d.getId() == id) {
                dishName.setText(d.getName());
                Glide.with(this).load(d.getImg()).into(dishImg);
                prepText.setText("Prep: " + String.valueOf(d.getPrep()) + " mins");
                cookText.setText("Cook: " + String.valueOf(d.getCook()) + " mins");
                totalText.setText("Total: " + String.valueOf(d.getTotal()) + " mins");
                servingText.setText("Serving: " + String.valueOf(d.getServing()));
                ingredientsDetails.setText(d.getIngredients());

            }
        }

    }



    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.backInstruc: {
                finish();
                break;
            }

            case R.id.nextnstruc: {
                Intent toInstruction = new Intent(Description.this,Instructions.class);
                toInstruction.putExtra("id", getIntent().getExtras().getInt("id"));
                startActivity(toInstruction);
                break;
            }
        }
    }
}