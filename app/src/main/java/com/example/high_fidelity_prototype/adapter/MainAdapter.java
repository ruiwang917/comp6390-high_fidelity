package com.example.high_fidelity_prototype.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.high_fidelity_prototype.R;
import com.example.high_fidelity_prototype.model.Dish;

import java.util.ArrayList;

public class MainAdapter extends RecyclerView.Adapter<MainAdapter.ViewHolder> {
    private ArrayList<Dish> dishList = new ArrayList<Dish>();
    private final OnItemClickListener onItemClickListener;

    public MainAdapter(ArrayList<Dish> dishList, OnItemClickListener onItemClickListener) {
        this.dishList = dishList;
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public MainAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.main_item, parent, false);
        return new ViewHolder(view, onItemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull MainAdapter.ViewHolder holder, int position) {
        Glide.with(holder.image.getContext()).load(dishList.get(position).getImg()).into(holder.image);
        holder.name.setText(dishList.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return dishList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView image;
        TextView name;
        OnItemClickListener onItemClickListener;
        public ViewHolder(@NonNull View itemView, OnItemClickListener onItemClickListener) {
            super(itemView);
            image = itemView.findViewById(R.id.dishImage);
            name = itemView.findViewById(R.id.dishName);
            this.onItemClickListener = onItemClickListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            onItemClickListener.onItemClick(getAdapterPosition());
        }
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }
}
