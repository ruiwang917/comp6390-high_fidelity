package com.example.high_fidelity_prototype.model;

import java.util.ArrayList;

public class Dish {
    private int id;
    private String name;                    // dish name



    private String img;                     // dish img
    private int prep;                       // preparation time
    private int cook;                       // cook time
    private int total;                      // total time
    private int serving;                    // number of servings
    private String ingredients;
    private ArrayList<String> instructions; // each step



    public Dish() {
    }

    public Dish(int id, String name, String img, int prep, int cook, int total, int serving, String ingredients, ArrayList<String> instructions) {
        this.id = id;
        this.name = name;
        this.img = img;
        this.prep = prep;
        this.cook = cook;
        this.total = total;
        this.serving = serving;
        this.ingredients = ingredients;
        this.instructions = instructions;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public int getPrep() {
        return prep;
    }

    public void setPrep(int prep) {
        this.prep = prep;
    }

    public int getCook() {
        return cook;
    }

    public void setCook(int cook) {
        this.cook = cook;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getServing() {
        return serving;
    }

    public void setServing(int serving) {
        this.serving = serving;
    }

    public String getIngredients() {
        return ingredients;
    }

    public void setIngredients(String ingredients) {
        this.ingredients = ingredients;
    }

    public ArrayList<String> getInstructions() {
        return instructions;
    }

    public void setInstructions(ArrayList<String> instructions) {
        this.instructions = instructions;
    }

}
