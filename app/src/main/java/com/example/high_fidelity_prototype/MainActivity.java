package com.example.high_fidelity_prototype;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.high_fidelity_prototype.adapter.MainAdapter;
import com.example.high_fidelity_prototype.data.DishData;
import com.example.high_fidelity_prototype.model.Dish;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements
        View.OnClickListener,
        MainAdapter.OnItemClickListener{
    //UI
    private MainAdapter mainAdapter;
    private RecyclerView mRV;

    //var
    DishData dishData = (DishData) this.getApplication();
    ArrayList<Dish> dishList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_main);

        mRV = findViewById(R.id.mRV);
        dishList = dishData.getDishes();
        initGridView();
    }

    private void initGridView() {
        GridLayoutManager gridLayoutManager = new GridLayoutManager(MainActivity.this,4,GridLayoutManager.VERTICAL,false);
        mRV.setLayoutManager(gridLayoutManager);
        mRV.setHasFixedSize(true);
        mainAdapter = new MainAdapter(dishList,this);
        mRV.setAdapter(mainAdapter);
    }



    @Override
    public void onClick(View view) {

    }

    @Override
    public void onItemClick(int position) {
        Intent toDescription = new Intent(MainActivity.this,Description.class);
        toDescription.putExtra("id", dishList.get(position).getId());
        startActivity(toDescription);

    }
}